package com.xp.consumer.test.listener;

import java.io.StringWriter;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xp.consumer.test.model.ConsumerProperties;
import com.xp.consumer.test.model.RecievedData;
import com.xp.consumer.test.repo.ConsumerPropertiesRepository;

@RestController
public class XPKafkaListener {

	@Autowired
	private ConsumerPropertiesRepository consumerPropertiesRepo;

	@KafkaListener(topics = "Test_Med_Aid_1", groupId = "group_id")
	public void consumeFile(@Payload RecievedData recievedData, @Header(KafkaHeaders.OFFSET) Long offset,
			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) String recievedPartitionId,
			@Header(KafkaHeaders.RECEIVED_TIMESTAMP) String recievedTimeStamp,
			@Header(KafkaHeaders.RECEIVED_TOPIC) String recievedTopic, Acknowledgment acknowledgement) {

		System.out.println(recievedData);
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(RecievedData.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(recievedData, sw);
			ConsumerProperties consumerProperties = new ConsumerProperties(BigInteger.valueOf(recievedData.getId()),
					recievedData.getAddress(), recievedData.getDob(), recievedData.getFile(), recievedData.getMobile(),
					recievedData.getName(), offset, Integer.parseInt(recievedPartitionId),
					Long.parseLong(recievedTimeStamp), recievedTopic.trim(), sw.toString());
			consumerPropertiesRepo.save(consumerProperties);
			acknowledgement.acknowledge();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@GetMapping("getAllconsumerLogs")
	public List<ConsumerProperties> getAllProducerLogs() {
		return consumerPropertiesRepo.findAll();
	}
}
