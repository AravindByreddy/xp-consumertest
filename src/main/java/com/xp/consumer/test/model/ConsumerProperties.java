package com.xp.consumer.test.model;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="consumerLog")
public class ConsumerProperties {
	@Id
	private BigInteger id;
	private String address;
	private Long dob;
	private List<FileData> file;
	private String mobile;
	private String name;
	private Long kafkaOffset;
	private Integer kafkaRecievedPartitionId;
	private Long kafkaRecievedTimeStamp;
	private String kafkaRecievedTopic;
	private String xml;
	
	public ConsumerProperties() {
	}

	public ConsumerProperties(BigInteger id, String address, Long dob, List<FileData> file, String mobile, String name,
			Long kafkaOffset, Integer kafkaRecievedPartitionId, Long kafkaRecievedTimeStamp,
			String kafkaRecievedTopic,String xml) {
		this.id = id;
		this.address = address;
		this.dob = dob;
		this.file = file;
		this.mobile = mobile;
		this.name = name;
		this.kafkaOffset = kafkaOffset;
		this.kafkaRecievedPartitionId = kafkaRecievedPartitionId;
		this.kafkaRecievedTimeStamp = kafkaRecievedTimeStamp;
		this.kafkaRecievedTopic = kafkaRecievedTopic;
		this.xml=xml;
	}
	
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getDob() {
		return dob;
	}
	public void setDob(Long dob) {
		this.dob = dob;
	}
	public List<FileData> getFile() {
		return file;
	}
	public void setFile(List<FileData> file) {
		this.file = file;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getKafkaOffset() {
		return kafkaOffset;
	}
	public void setKafkaOffset(Long kafkaOffset) {
		this.kafkaOffset = kafkaOffset;
	}
	public Integer getKafkaRecievedPartitionId() {
		return kafkaRecievedPartitionId;
	}
	public void setKafkaRecievedPartitionId(Integer kafkaRecievedPartitionId) {
		this.kafkaRecievedPartitionId = kafkaRecievedPartitionId;
	}
	public Long getKafkaRecievedTimeStamp() {
		return kafkaRecievedTimeStamp;
	}
	public void setKafkaRecievedTimeStamp(Long kafkaRecievedTimeStamp) {
		this.kafkaRecievedTimeStamp = kafkaRecievedTimeStamp;
	}
	public String getKafkaRecievedTopic() {
		return kafkaRecievedTopic;
	}
	public void setKafkaRecievedTopic(String kafkaRecievedTopic) {
		this.kafkaRecievedTopic = kafkaRecievedTopic;
	}
	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	@Override
	public String toString() {
		return "ConsumerProperties [id=" + id + ", address=" + address + ", dob=" + dob + ", file=" + file + ", mobile="
				+ mobile + ", name=" + name + ", kafkaOffset=" + kafkaOffset + ", kafkaRecievedPartitionId="
				+ kafkaRecievedPartitionId + ", kafkaRecievedTimeStamp=" + kafkaRecievedTimeStamp
				+ ", kafkaRecievedTopic=" + kafkaRecievedTopic + ", xml=" + xml + "]";
	}
}
