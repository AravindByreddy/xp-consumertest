package com.xp.consumer.test.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD)
public class RecievedData {
	
	private Integer id;
	private String name;
	private Long dob;
	private String address;
	private String mobile;
	private List<FileData> file;
	
	public RecievedData() {
	}
	public RecievedData(Integer id, String name, Long dob, String address, String mobile, List<FileData> file) {
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.address = address;
		this.mobile = mobile;
		this.file = file;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getDob() {
		return dob;
	}
	public void setDob(Long dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public List<FileData> getFile() {
		return file;
	}
	public void setFile(List<FileData> file) {
		this.file = file;
	}
	@Override
	public String toString() {
		return "RecievedData [id=" + id + ", name=" + name + ", dob=" + dob + ", address=" + address + ", mobile="
				+ mobile + ", file=" + file + "]";
	}
	
	}
