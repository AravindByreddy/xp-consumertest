package com.xp.consumer.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XpConsumerTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(XpConsumerTestApplication.class, args);
	}

}
