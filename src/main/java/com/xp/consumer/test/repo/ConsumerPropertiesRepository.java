package com.xp.consumer.test.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xp.consumer.test.model.ConsumerProperties;

public interface ConsumerPropertiesRepository extends MongoRepository<ConsumerProperties, Integer> {


}
